var dao = require('./dao.js');
var config = require('./config.js');
var fs = require('fs');

var index = config.date.from;
var prevValue = config.sensor_data.startvalue;

var StringBuffer = function() {
    this.buffer = [];
    this.index = 0;
};
 
StringBuffer.prototype = {
    append: function(s) {
        this.buffer[this.index] = s;
        this.index += 1;
        return this;
    },
     
    toString: function() {
        return this.buffer.join('');
    }
};

var log = new StringBuffer();

var appendLog = function(str) {
    log.append(str + '\n');
}

var saveLog = function(callback) {
    fs.writeFile('log2.txt', log.toString(), (err) => {
        console.log('Log appednd');
        callback(err);
    });
}

var generateDataFromDateRange = function() {
    var random_floor = Math.random();
    var random = generateRandomValueFromRange(-random_floor, random_floor, prevValue);
    var sensor_value = (prevValue + random).toPrecision(2);
    var sensor_date = new Date(index);
    console.log('Random value: ' + random);
    console.log('Value: ' + sensor_value + " Date: " + sensor_date.toLocaleDateString() + ' ' + sensor_date.toLocaleTimeString());
    saveSesorData(sensor_value, index).then( date => {
        index = date;
        prevValue = Number(sensor_value);
        appendLog(('Random value: ' + random + '\nValue: ' + sensor_value + " Date: " +
        sensor_date.toLocaleDateString() + ' ' + sensor_date.toLocaleTimeString()));
        saveLog(() => {
            generateDataFromDateRange();
        });
    }).catch(err => {});
}

var generateRandomValueFromRange = function(min, max, prev) {
    var random = (min + Math.random() * (max - min));
    return ((prev + random) >= config.sensor_data.startvalue &&
            (prev + random) <= config.sensor_data.maxvalue) ? random : generateRandomValueFromRange(min, max, prev);
}

var saveSesorData = function(value, date) {
    return new Promise((resolve, reject) => {
        dao.updateSensorsData(config.sensor_id, value, date, (err, sensor) => {
            if (err) {
                console.log('Error: ' + err);
            }
            else {
                console.log('Updated!');
                appendLog('Sensor updated');
                var new_date = date + (60 * 1000 * config.date.timeinterval);
                resolve(new_date);
            }
        });
    });
}


var start = function() {
    console.log('==============================================================')
    console.log('Generating data for sensor: ' + config.sensor_id);
    console.log('Initial date: ' + new Date(config.date.from).toLocaleDateString());
    console.log('Final date: ' + new Date(config.date.to).toLocaleDateString());
    console.log('==============================================================');

    appendLog('==============================================================');
    appendLog('Generating data for sensor: ' + config.sensor_id);
    appendLog('Initial date: ' + new Date(config.date.from).toLocaleDateString());
    appendLog('Final date: ' + new Date(config.date.to).toLocaleDateString());
    appendLog('==============================================================');

    generateDataFromDateRange();
}

/**
 * Run script
 */

start();
