# RTMS Sensor Data Generator

Generate data from date range and save it to sensor with given ID

## Getting started 

Configure script by modyfing config.js module. Date from, to and data value range can be adjusted

## Running script 

Script can be run by calling 

```
node main
```
### Logging
Script generates log. Log file name is set in config.js