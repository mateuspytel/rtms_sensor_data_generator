"use strict"
var mongoose = require('mongoose');
var model = require('./model.js');
var config = require('./config.js');

/*
*   Connect to MongoDB
*/
mongoose.Promise = global.Promise;
mongoose.connect(config.mongoURI)
.then( () => {
    console.log('Successfully connected to MongoDB');
}
,err => {
    console.log('Error. Connection with MongoDB failed: Error: ' + err);
});

module.exports = {
    /**
     * Saves new user in MongoDB and returns newly created object and error via callback function
     * @param {object} sensor
     * @param {function} callback
     */
    createSensor(sensor, callback) {
        model.Sensor.create(sensor, (err, s) => {
            callback(err, s);
        });
    },

    /**
     * Removes sensor from DB
     * @param {string} id sensor id
     * @param {function} callback
     */
    removeSensor(id, callback) {
        model.Sensor.remove({_id: id}, (err) => {
            callback(err);
        });
    },

    /**
     * Return only sensors data 
     * @param {string} sensor_id
     * @param {function} callback
     */
    getSensorsData(sensor_id, callback) {
        model.Sensor.findOne({_id : sensor_id}, '_id data lastmodified active_since alerts localization active name type', (err, sensor) => {
            callback(err, sensor);
        });
    },

    /**
     * Update sensors data
     * @param {string} sensor_id
     * @param {number} value
     * @param {number} date
     * @param {function} callback
     */
    updateSensorsData(sensor_id, value, date, callback) {
        model.Sensor.findOneAndUpdate({_id : sensor_id, active: true}, 
            {
                $addToSet: { data: {date: date, value: value}},
                $set: {lastmodified : Date.now()},
            }, {new: true}, (err, sensor) => {
                if (sensor && sensor.data && sensor.data.length > 0) {
                    model.Sensor.findOneAndUpdate({_id: sensor_id, active_since: {$exists : false}},
                    {
                        $set: {active_since: sensor.data[0].date}
                    }, {new: true}, (err, s) => {});
                }
                callback(err, sensor);
            });
    },

    /**
     * Updates sensors details
     * @param {object} sensor
     * @param {function} callback
     */
    updateSensorsDetails(sensor, callback) {
        model.Sensor.findOneAndUpdate({_id : sensor._id},
        {
            $set : {
                type : sensor.type,
                name : sensor.name,
                localization : sensor.localization,
                active : sensor.active,
                alerts: sensor.alerts
            }
        }, 
        {
            new: true,
            fields: {'_id' : 1, 'registered_at' : 1, 'type' : 1, 'localization' : 1, 'active_since' : 1, 'lastmodified' : 1,
            'alerts' : 1}
        }
        ,(err, sensor) => {
            callback(err, sensor);
        });
    }
};